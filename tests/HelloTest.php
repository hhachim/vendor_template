<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use myCompany\MyPackageName\Hello;

final class HelloTest extends TestCase
{
    public function testCanSaySomething(): void
    {
        $helloClass = new Hello();
        $message = "Hello world";
        $this->assertEquals(
            $message,
            $helloClass->say($message)
        );
    }
}