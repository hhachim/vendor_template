.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

PROJECTS_DIR := /home/hachimhassani/projects

## Install
install: ## Install or reset monolith
	docker-compose up -d
	docker-compose run php bash -c "composer install"
test: ## Testing the package
	docker-compose run php bash -c "./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests"

test-hello: ## testing just on class
	docker-compose run php bash -c "./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Hello.php"
down: ## remove all containers and their data
	docker-compose down

bash: ## enter in the container
	docker-compose run php bash