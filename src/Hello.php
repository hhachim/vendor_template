<?php 

/*
 * (c) YOUR NAME <your@email.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

// the company in camelCase
// and the package in PascalCase
namespace myCompany\MyPackageName;

class Hello
{
    public function say($toSay = "Nothing given")
    {
        return $toSay;
    }
}